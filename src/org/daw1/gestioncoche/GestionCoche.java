/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestioncoche;

import org.daw1.gestioncoche.clases.Coche;
import org.daw1.gestioncoche.clases.TipoCombustible;
/**
 *
 * @author rgcenteno
 */
public class GestionCoche {

    /**
     * @param args the command line arguments
     */
    
    private static java.util.Scanner teclado;
    private static java.util.Map<String, Coche> coches;
    
    public static void main(String[] args) {        
        teclado = new java.util.Scanner(System.in);
        coches = new java.util.TreeMap<>();
        String input = "";
        do{
            System.out.println("******************************************");
            System.out.println("* 1. Crear coche                         *");
            System.out.println("* 2. Mostrar matrículas coches existentes*");
            System.out.println("* 3. Mostrar información coche           *");           
            System.out.println("* 4. Eliminar coche                      *");
            System.out.println("*                                        *");
            System.out.println("* 0. Salir                               *");
            System.out.println("******************************************");
            input = teclado.nextLine();
            switch(input){
                case "1":                    
                    //Creamos un coche y si no existe la matrícula, lo añadimos
                    Coche c = crearCoche();
                    if(c != null){
                        coches.put(c.getMatricula(), c);
                    }
                    break;
                case "2":
                    //Recorremos el map mostrando sólo las matrículas de los coches
                    for(String m : coches.keySet()){
                        System.out.println(m);
                    }
                    break;
                case "3":
                    //Pedimos la matrícula y si existe mostramos los datos del coche (todos los datos en una string)   
                    System.out.println("Por favor inserte la matrícula del coche que desea visualizar");
                    String m = teclado.nextLine();
                    if(coches.containsKey(m)){
                        Coche aux = coches.get(m);
                        System.out.println("Marca: " + aux.getMarca() + "\nModelo: " + aux.getModelo() + "\nMatrícula: " + aux.getMatricula() + "\nNum. Bastidor: " + aux.getNumBastidor() + "\nTipoCombustible: " + aux.getTipoCombustible() + "\nEléctrico?: " + (aux.isElectrico() ? "Sí" : "No"));
                        break;
                    }
                    else{
                        System.out.println("El coche no existe");
                    }
                    break;
                case "4":
                    //Pedimos matrícula y borramos el coche si existe   
                    System.out.println("Por favor inserte la matrícula del coche que desea borrar");
                    m = teclado.nextLine();
                    if(coches.containsKey(m)){
                        coches.remove(m);
                        System.out.println("Elemento borrado correctamente");
                    }
                    else{
                        System.out.println("El coche no existe");
                    }
                case "0":
                    break;
                default:
                    System.out.println("Opción inválida");
                    break;
            }
        }
        while(!input.equals("0"));
    }
    
    public static Coche crearCoche(){        
        String matricula = solicitarStringMatches("Por favor inserte la matrícula del coche", "Formato incorrecto. Formato válido 0000AAA", Coche.PATRON_MATRICULA);
        
        if(coches.containsKey(matricula)){
            System.out.println("El coche que está intentando crear ya existe. Abortando...");
            return null;
        }
        //Si llegamos aquí la matrícula no existe y podemos pedir el resto de datos
        String marca = solicitarStringMatches("Inserte la marca del coche", "Sólo se permiten letras, números y espacios", Coche.PATRON_MARCA_MODELO);        
        String modelo = solicitarStringMatches("Inserte la modelo del coche", "Sólo se permiten letras, números y espacios", Coche.PATRON_MARCA_MODELO);        
        String numBastidor = solicitarStringMatches("Inserte el número de bastidor del coche", "Error en el formato. ABCDEF1ABCD123456", Coche.PATRON_BASTIDOR);
        
        TipoCombustible tipoCombustible = null;
        Boolean electrico = null;
        do{            
            do{
                System.out.println("Inserte el tipo de combustible");
                int aux = 1;
                for(TipoCombustible t : TipoCombustible.values()){
                    System.out.println(aux + ". " + t);
                    aux++;
                }
                if(teclado.hasNextInt()){
                    int temp = teclado.nextInt();
                    if(temp >= 1 && temp <= TipoCombustible.values().length){
                        tipoCombustible = TipoCombustible.of(temp);
                    }
                    else{
                        System.out.println("Inserte un número válido");
                    }
                }
                teclado.nextLine();
            }
            while(tipoCombustible == null);
            
            do{
                System.out.println("¿Es eléctrico? (s)i (n)o");
                String opcion = teclado.nextLine();
                if(opcion.equalsIgnoreCase("s")){
                    electrico = true;
                }
                else if(opcion.equalsIgnoreCase("n")){
                    electrico = false;
                }
            }
            while(electrico == null);
            if(!electrico && tipoCombustible == TipoCombustible.NINGUNO){
                System.out.println("El coche debe usar algún tipo de combustible o ser eléctrico");
            }
        }
        while(!electrico && tipoCombustible == TipoCombustible.NINGUNO);
        
        return new Coche(marca, modelo, matricula, numBastidor, tipoCombustible, electrico);
    }
    
    public static String solicitarStringMatches(String textoPregunta, String textoError, java.util.regex.Pattern p){
        String aux = "";
        do{
            System.out.println(textoPregunta);
            aux = teclado.nextLine();
            if(!p.matcher(aux).matches()){
                System.out.println(textoError);
            }
        }
        while(!p.matcher(aux).matches());
        return aux;
    }
    
    /*public static String solicitarMarcaModelo(String textoSolicitud){
        String txt = "";
        do{
            System.out.println(textoSolicitud);
            txt = teclado.nextLine();
        }
        while(txt.isBlank());
        return txt;
    }*/
    
   
}
