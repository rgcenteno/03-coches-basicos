/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestioncoche.clases;

/**
 *
 * @author rgcenteno
 */
public enum TipoCombustible {
    NINGUNO, DIESEL, GASOLINA;
    
    public static TipoCombustible of(int tipo) {
        if (tipo < 1 || tipo > TipoCombustible.values().length) {
            throw new IllegalArgumentException("Valor no válido");
        }
        return TipoCombustible.values()[tipo - 1];

    }
    
}
