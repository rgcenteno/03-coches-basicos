/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestioncoche.clases;

import java.util.regex.Pattern;
/**
 *
 * @author rgcenteno
 */
public class Coche implements Comparable<Coche>{
    
    private final String marca;
    private final String modelo;
    private String matricula;
    private final String numBastidor;
    private final TipoCombustible tipoCombustible;
    private final boolean electrico;
    
    final public static Pattern PATRON_MATRICULA = Pattern.compile("[0-9]{4}[A-Z]{3}");
    final public static Pattern PATRON_MARCA_MODELO = Pattern.compile("[A-Za-z0-9 ]+");
    final public static Pattern PATRON_BASTIDOR = Pattern.compile("[A-Z]{6}[0-9]{1}[A-Z]{4}[0-9]{6}");

    public Coche(String marca, String modelo, String matricula, String numBastidor, TipoCombustible tipoCombustible, boolean electrico) {        
        checkMarcaModelo(marca);
        checkMarcaModelo(modelo);
        checkMatricula(matricula);
        checkNumBastidor(numBastidor);
        checkTipoCombustibleElectrico(tipoCombustible, electrico);
        this.marca = marca;
        this.modelo = modelo;
        this.matricula = matricula;
        this.numBastidor = numBastidor;
        this.tipoCombustible = tipoCombustible;
        this.electrico = electrico;
    }
    
    private static void checkMarcaModelo(String texto){
        if(texto == null || !PATRON_MARCA_MODELO.matcher(texto).matches()){
            throw new IllegalArgumentException("Sólo se permiten letras, números y espacios en marca y modelo");
        }
    }
    
    private static void checkMatricula(String matricula){        
        if(matricula == null || !PATRON_MATRICULA.matcher(matricula).matches()){
            throw new IllegalArgumentException("La matrícula debe seguir el formato 1234ABC");
        }
    }
    
    private static void checkNumBastidor(String numBastidor){
        if(numBastidor == null || !PATRON_BASTIDOR.matcher(numBastidor).matches()){
            throw new IllegalArgumentException("El número de bastidor sigue el formato ABCDEF1ABCD123456");
        }
    }
    
    private static void checkTipoCombustibleElectrico(TipoCombustible combustible, boolean electrico){
        if(combustible == null || (!electrico && combustible == TipoCombustible.NINGUNO)){
            throw new IllegalArgumentException("El vehículo debe consumir algún tipo de energía");
        }                
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getNumBastidor() {
        return numBastidor;
    }

    public TipoCombustible getTipoCombustible() {
        return tipoCombustible;
    }

    public boolean isElectrico() {
        return electrico;
    }

    public void setMatricula(String matricula) {
        checkMatricula(matricula);
        this.matricula = matricula;
    }
    
    public String getMarcaModelo(){
        return this.marca + " " + this.modelo;
    }

    @Override
    public String toString() {
        return this.getMarcaModelo() + " " + this.matricula; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Coche t) {
        if(t == null){
            throw new NullPointerException();
        }
        else{
            return this.matricula.compareTo(t.matricula);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else if(obj == this){
            return true;
        }
        else if(obj instanceof Coche){
            Coche aux = (Coche)obj;
            return this.matricula.equals(aux.matricula);
        }
        else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.matricula.hashCode();
    }
    
    
    
    
}
