03 - Crea una clase Coche que almacene los siguientes datos:

    Marca
    Modelo
    Matrícula
    Num bastidor (6 mayúsculas - 1 números - 4 mayúsculas - 6 números)
    Tipo combustible (Diesel | Gasolina | Ninguno)
    Eléctrico (Sí / No)

Crea un constructor, los métodos getter de todos los atributos y los setter (validados) de los atributos que el programa debería dejar cambiar. Todo coche debe tener algún método de sumistrar energía (o un combustible o es eléctrico o ambos). A mayores, crea un método que devuelva el modelo completo (marca+modelo). Sobreescribe toString() para que muestre marca + modelo + matrícula. Sobreescribe compareTo para que ordene por defecto a los coches por matrícula. Sobreescribe equals para que sea igual si la matrícula. Sobreescribe hashcode teniendo en cuenta esto.
